#include "liste.hpp"

#include <iostream>
#include <cassert>


Liste::Liste() {
  nb_elements = 0;
  premiere = nullptr;
}

Liste::Liste(const Liste& autre) {

  nb_elements = 0;
  premiere = nullptr;

  const Cellule * parcours;
  parcours = autre.tete();

  while (parcours != nullptr) {
    this->ajouter_en_queue(parcours->valeur);
    parcours = parcours-> suivante;

  }

}

Liste& Liste::operator=(const Liste& autre) {
  const Cellule * parcours = autre.tete();
  while (tete() != nullptr) {
    supprimer_en_tete();
  }
  while (parcours != nullptr) {
    ajouter_en_queue(parcours->valeur);
    parcours = parcours->suivante;
  }
  return *this ;
}

Liste::~Liste() {

  Cellule * tmp ;

  while (premiere != nullptr) {
    tmp = premiere;
    premiere = premiere->suivante;
    delete tmp;
  }
  delete premiere;
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule* tmp = new Cellule;
  tmp->valeur = valeur;
  tmp->suivante = this->premiere;
  this->premiere = tmp;
  nb_elements++;
}

void Liste::ajouter_en_queue(int valeur) {
  if(premiere != nullptr){
    Cellule * last = new Cellule;
    last->valeur = valeur;
    last->suivante = nullptr;
    queue()->suivante = last;
    nb_elements++;
  }
  else ajouter_en_tete(valeur);

}

void Liste::supprimer_en_tete() {
  if (premiere != nullptr){
    Cellule * tmp = premiere;
    premiere = tmp->suivante;
    nb_elements--;
    delete tmp;
  }
}

Cellule* Liste::tete() {
  return premiere;
}

const Cellule* Liste::tete() const {
  return premiere ;
}

Cellule* Liste::queue() {
  Cellule * parcours = this->premiere;
  while(parcours-> suivante != nullptr){
    parcours = parcours->suivante;
  }
  return parcours;
}

const Cellule* Liste::queue() const {
  Cellule * parcours = this->premiere;
  while(parcours-> suivante != nullptr){
    parcours = parcours->suivante;
  }
  return parcours;
}

int Liste::taille() const {
  return nb_elements;
}

Cellule* Liste::recherche(int valeur) {
  Cellule * parcours = premiere;

  while (parcours != nullptr) {
    if(parcours->valeur == valeur) return parcours;
    parcours = parcours->suivante;
  }
  return parcours ;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule * parcours = premiere;

  while (parcours != nullptr) {
    if(parcours->valeur == valeur) return parcours;
    parcours = parcours->suivante;
  }
  return parcours ;
}

void Liste::afficher() const {
  Cellule * parcours = this->premiere;
  int i = 0;
  while(parcours != nullptr){
    std::cout<< "Cellule "<< i <<" : "<< parcours->valeur << " , ";
    parcours = parcours->suivante;
    i++;
  }
  std::cout<<std::endl;

}
